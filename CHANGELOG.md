# [v1.6](https://gitlab.com/derSchabi/OCBookmarks/tags/v1.6)

- Latest target version update
- Fix icons
- Gradle update.

# [v1.5](https://gitlab.com/derSchabi/OCBookmarks/tags/v1.5)

- Fix several translation
- Fix icons


# [v1.2](https://gitlab.com/derSchabi/OCBookmarks/tags/v1.2)

### Fix
- Fixed icons in menu
- fixed following / problem in log in


# [v1.1](https://gitlab.com/derSchabi/OCBookmarks/tags/v1.1)

### New
- Add support for owncloud (just the cooperate identity).
- add proguard (minify)
- Russian translation

### fixes
- spelling fixes
- allow to update tags


# [v1.0](https://gitlab.com/derSchabi/OCBookmarks/tags/v1.0)
Initial release providing general functionality.